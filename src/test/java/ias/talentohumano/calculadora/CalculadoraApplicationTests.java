package ias.talentohumano.calculadora;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CalculadoraApplicationTests {

	/*@Autowired
	ServiciosService serviciosService;

	@Autowired
	TecnicosService tecnicosService;

	@Autowired
	ReporteController reporteController;

	@Test
	void getTotalServices() {
		assertEquals(3, serviciosService.getServicios().size());
	}

	@Test
	void getValidTecnicoNameById() {
		assertEquals("JONATHAN INSUASTY", tecnicosService.getTecnicoById("CC12345678").get().getNombre());
	}

	@Test
	void addReportWithWrongIdtecnico () {
		String iniciostr = "2021-03-04T11:30:02";
		String finstr = "2021-03-04T11:39:02";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
		LocalDateTime inicio = LocalDateTime.parse(iniciostr, formatter);
		LocalDateTime fin = LocalDateTime.parse(finstr, formatter);

		ReporteModel reporte = new ReporteModel();
		reporte.setIdtecnico("CC1087960237");
		reporte.setIdservicio("101");
		reporte.setFechainicio(inicio);
		reporte.setFechafin(fin);
		assertEquals(ResponseEntity.badRequest().body("{\"error\":\"El documento del técnico ingresado no existe.\"}"),reporteController.addReporte(reporte));
	}

	@Test
	void addReporteWithWrongRangeDates() {
		String iniciostr = "2021-03-04T11:30:02";
		String finstr = "2021-03-03T11:39:02";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
		LocalDateTime inicio = LocalDateTime.parse(iniciostr, formatter);
		LocalDateTime fin = LocalDateTime.parse(finstr, formatter);

		ReporteModel reporte = new ReporteModel();
		reporte.setIdtecnico("CC12345678");
		reporte.setIdservicio("101");
		reporte.setFechainicio(inicio);
		reporte.setFechafin(fin);
		assertEquals(ResponseEntity.badRequest().body("{\"error\":\"La fecha final debe ser mayor a la fecha inicial.\"}"),reporteController.addReporte(reporte));
	}*/

}
