package ias.talentohumano.calculadora.reporte.services;

import ias.talentohumano.calculadora.reporte.models.ServicioModel;
import ias.talentohumano.calculadora.reporte.repositories.ServicioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class ServiciosService {
    @Autowired
    ServicioRepository servicioRepository;

    public ArrayList<ServicioModel> getServicios(){
        return (ArrayList<ServicioModel>) servicioRepository.findAll();
    }
}
