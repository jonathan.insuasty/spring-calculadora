package ias.talentohumano.calculadora.reporte.services;

import ias.talentohumano.calculadora.reporte.models.TecnicoModel;
import ias.talentohumano.calculadora.reporte.repositories.TecnicoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TecnicosService {
    @Autowired
    TecnicoRepository tecnicoRepository;

    public Optional<TecnicoModel> getTecnicoById(String idtecnico) {
        return tecnicoRepository.findById(idtecnico);
    }
}
