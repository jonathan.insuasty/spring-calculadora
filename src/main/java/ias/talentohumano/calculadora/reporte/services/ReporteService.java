package ias.talentohumano.calculadora.reporte.services;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ias.talentohumano.calculadora.reporte.models.HorasResponse;
import ias.talentohumano.calculadora.reporte.models.ReporteModel;
import ias.talentohumano.calculadora.reporte.models.TecnicoModel;
import ias.talentohumano.calculadora.reporte.repositories.ReporteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Optional;

import static java.time.DayOfWeek.SUNDAY;

@Service
public class ReporteService {

    @Autowired
    ReporteRepository reporteRepository;

    @Autowired
    TecnicosService tecnicosService;

    public ReporteModel addReporte(ReporteModel reporte) {
        reporte.setFechainicio(reporte.getFechainicio().minusHours(5));
        reporte.setFechafin(reporte.getFechafin().minusHours(5));
        return reporteRepository.save(reporte);
    }

    public String getReporteByTecnicoAndWeek(String idtecnico, String week) throws JsonProcessingException {
        LocalDate firstWeekDay = LocalDate.parse("2021-W" + week + "-1", DateTimeFormatter.ISO_WEEK_DATE);
        LocalDate lastWeekDay = firstWeekDay.plusDays(7);

        ArrayList<ReporteModel> reportes = reporteRepository.getReporteByTecnicoAndWeek(idtecnico, firstWeekDay.toString(), lastWeekDay.toString());
        Optional<TecnicoModel> tecnico = tecnicosService.getTecnicoById(idtecnico);

        String nombretecnico;
        if (tecnico.isEmpty()) {
            nombretecnico = "";
        } else {
            nombretecnico = tecnico.get().getNombre();
        }

        float normalHours = 0;
        float nightHours = 0;
        float sundayHours = 0;
        float normalExtraHours = 0;
        float nightExtraHours = 0;
        float sundayExtraHours = 0;

        for (ReporteModel reporte: reportes) {
            normalHours += normalHoursDiference(reporte.getFechainicio(), reporte.getFechafin());
            nightHours += nightHoursDiference(reporte.getFechainicio(), reporte.getFechafin());
            sundayHours += sundayHoursDiference(reporte.getFechainicio(), reporte.getFechafin());
        }

        if (normalHours > 48){
            normalExtraHours = normalHours - 48;
            normalHours = 48;
        }

        if (nightHours > 48){
            nightExtraHours = nightHours - 48;
            nightHours = 48;
        }

        if (sundayHours > 48){
            sundayExtraHours = sundayHours - 48;
            sundayHours = 48;
        }

        HorasResponse workedHours = new HorasResponse(idtecnico, nombretecnico, week, firstWeekDay.toString(), lastWeekDay.minusDays(1).toString(), normalHours, nightHours, sundayHours, normalExtraHours, nightExtraHours, sundayExtraHours);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        return objectMapper.writeValueAsString(workedHours);
    }

    private Float normalHoursDiference(LocalDateTime inicio, LocalDateTime fin) {

        if(inicio.getDayOfWeek()==SUNDAY) {
            return Float.valueOf(0);
        } else {
            LocalDateTime modifiedFin;
            if(fin.getHour() >= 20) {
                modifiedFin = fin.withHour(20).withMinute(0).withSecond(0);
            } else {
                modifiedFin = fin;
            }

            Long remainingSeconds;
            Float normalHours;

            remainingSeconds = ChronoUnit.SECONDS.between(inicio, modifiedFin);
            normalHours = remainingSeconds.floatValue()/3600;

            return normalHours;
        }
    }

    private Float nightHoursDiference(LocalDateTime inicio, LocalDateTime fin) {

        if(inicio.getDayOfWeek()==SUNDAY) {
            return Float.valueOf(0);
        } else {
            LocalDateTime modifiedInicio;
            if(fin.getHour() >= 20) {
                modifiedInicio = inicio.withHour(20).withMinute(0).withSecond(0);
            } else {
                modifiedInicio = fin;
            }

            Long remainingSeconds;
            Float nightHours;

            remainingSeconds = ChronoUnit.SECONDS.between(modifiedInicio, fin);
            nightHours = remainingSeconds.floatValue()/3600;

            return nightHours;
        }
    }

    private Float sundayHoursDiference(LocalDateTime inicio, LocalDateTime fin) {

        if(inicio.getDayOfWeek()==SUNDAY) {

            Long remainingSeconds;
            Float nightHours;

            remainingSeconds = ChronoUnit.SECONDS.between(inicio, fin);
            nightHours = remainingSeconds.floatValue()/3600;

            return nightHours;

        } else {
            return Float.valueOf(0);
        }


    }
}
