package ias.talentohumano.calculadora.reporte.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "servicios")
public class ServicioModel {

    @Id
    @Column(nullable = false, unique = true)
    private Long idservicio;
    private String descripcion;

    public void setIdservicio(Long idservicio) {
        this.idservicio = idservicio;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public Long getIdservicio() {
        return this.idservicio;
    }

}
