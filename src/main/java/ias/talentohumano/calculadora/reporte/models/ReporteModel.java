package ias.talentohumano.calculadora.reporte.models;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "reportes")
public class ReporteModel {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true, nullable = false)
    private Long idreporte;

    @Column(nullable = false)
    private String idtecnico;

    @Column(nullable = false)
    private String idservicio;

    @Column(nullable = false)
    private LocalDateTime fechainicio;

    @Column(nullable = false)
    private LocalDateTime fechafin;

    public void setIdreporte(Long idreporte) {
        this.idreporte = idreporte;
    }

    public void setIdtecnico(String idtecnico) {
        this.idtecnico = idtecnico;
    }

    public void setIdservicio(String idservicio) {
        this.idservicio = idservicio;
    }

    public void setFechainicio(LocalDateTime fechainicio) {
        this.fechainicio = fechainicio;
    }

    public void setFechafin(LocalDateTime fechafin) {
        this.fechafin = fechafin;
    }

    public Long getIdreporte() {
        return this.idreporte;
    }

    public String getIdtecnico() {
        return this.idtecnico;
    }

    public String getIdservicio() {
        return this.idservicio;
    }

    public LocalDateTime getFechainicio() {
        return this.fechainicio;
    }

    public LocalDateTime getFechafin() {
        return this.fechafin;
    }

}
