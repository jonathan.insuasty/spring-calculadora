package ias.talentohumano.calculadora.reporte.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tecnicos")
public class TecnicoModel {

    @Id
    @Column(nullable = false, unique = true)
    private String idtecnico;
    private String nombre;

    public void setIdtecnico(String idtecnico) {
        this.idtecnico = idtecnico;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getIdtecnico() {
        return this.idtecnico;
    }

}
