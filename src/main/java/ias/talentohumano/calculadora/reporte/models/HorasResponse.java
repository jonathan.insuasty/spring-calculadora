package ias.talentohumano.calculadora.reporte.models;


public class HorasResponse {
    private String idtecnico;
    private String nombretecnico;
    private String week;
    private String fechaInicial;
    private String fechaFinal;
    private Float normalHours;
    private Float nightHours;
    private Float sundayHours;
    private Float normalExtraHours;
    private Float nightExtraHours;
    private Float sundayExtraHours;

    public HorasResponse() {
        super();
    }

    public HorasResponse(String idtecnico, String nombretecnico, String week, String fechaInicial, String fechaFinal, Float normalHours, Float nightHours, Float sundayHours, Float normalExtraHours, Float nightExtraHours, Float sundayExtraHours){
        super();
        this.idtecnico = idtecnico;
        this.nombretecnico = nombretecnico;
        this.week = week;
        this.normalHours = normalHours;
        this.nightHours = nightHours;
        this.sundayHours = sundayHours;
        this.normalExtraHours = normalExtraHours;
        this.nightExtraHours = nightExtraHours;
        this.sundayExtraHours = sundayExtraHours;
        this.fechaInicial = fechaInicial;
        this.fechaFinal = fechaFinal;
    }

    public void setIdtecnico(String idtecnico) {
        this.idtecnico = idtecnico;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public void setNormalHours(Float normalHours) {
        this.normalHours = normalHours;
    }

    public void setNightHours(Float nightHours) {
        this.nightHours = nightHours;
    }

    public void setSundayHours(Float sundayHours) {
        this.sundayHours = sundayHours;
    }

    public void setNormalExtraHours(Float normalExtraHours) {
        this.normalExtraHours = normalExtraHours;
    }

    public void setNightExtraHours(Float nightExtraHours) {
        this.nightExtraHours = nightExtraHours;
    }

    public void setSundayExtraHours(Float sundayExtraHours) {
        this.sundayExtraHours = sundayExtraHours;
    }

    public String getIdtecnico(String idtecnico) {
        return this.idtecnico;
    }

    public String getWeek(String week) {
        return this.week;
    }

    public Float getNormalHours(Float normalHours) {
        return this.normalHours;
    }

    public Float getNightHours(Float nightHours) {
        return this.nightHours;
    }
    public Float getSundayHours(Float sundayHours) {
        return this.sundayHours;
    }

    public Float getNormalExtraHours(Float normalExtraHours) {
        return this.normalExtraHours;
    }

    public Float getNightExtraHours(Float nightExtraHours) {
        return this.nightExtraHours;
    }
    public Float getSundayExtraHours(Float sundayExtraHours) {
        return this.sundayExtraHours;
    }

}
