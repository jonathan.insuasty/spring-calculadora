package ias.talentohumano.calculadora.reporte.repositories;

import ias.talentohumano.calculadora.reporte.models.TecnicoModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TecnicoRepository extends CrudRepository<TecnicoModel, String> {
}
