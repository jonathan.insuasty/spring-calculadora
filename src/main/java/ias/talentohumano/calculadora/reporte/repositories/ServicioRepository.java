package ias.talentohumano.calculadora.reporte.repositories;

import ias.talentohumano.calculadora.reporte.models.ServicioModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ServicioRepository extends CrudRepository<ServicioModel, Long> {
}
