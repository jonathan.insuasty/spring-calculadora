package ias.talentohumano.calculadora.reporte.repositories;

import ias.talentohumano.calculadora.reporte.models.ReporteModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;

@Repository
public interface ReporteRepository extends CrudRepository<ReporteModel, Long> {
    @Query(value = "SELECT reportes.*, tecnicos.nombre FROM reportes, tecnicos WHERE tecnicos.idtecnico = reportes.idtecnico AND reportes.idtecnico = ?1 AND fechainicio >= ?2 AND fechafin < ?3", nativeQuery = true)
    ArrayList<ReporteModel> getReporteByTecnicoAndWeek(String idtecnico, String diainicial, String diafinal);
}
