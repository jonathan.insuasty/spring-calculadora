package ias.talentohumano.calculadora.reporte.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import ias.talentohumano.calculadora.reporte.models.ReporteModel;
import ias.talentohumano.calculadora.reporte.models.TecnicoModel;
import ias.talentohumano.calculadora.reporte.services.ReporteService;
import ias.talentohumano.calculadora.reporte.services.TecnicosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/reporteServicio")
public class ReporteController {

    @Autowired
    ReporteService reporteService;

    @Autowired
    TecnicosService tecnicosService;

    @PostMapping(produces = "application/json")
    public ResponseEntity addReporte(@RequestBody ReporteModel reporte) {
        Optional<TecnicoModel> tecnico = tecnicosService.getTecnicoById(reporte.getIdtecnico());
        if(tecnico.isEmpty()){
            return ResponseEntity.badRequest().body("{\"error\":\"El documento del técnico ingresado no existe.\"}");
        }
        if(reporte.getFechafin().isAfter(reporte.getFechainicio())) {
            return ResponseEntity.ok(reporteService.addReporte(reporte));
        } else {
            return ResponseEntity.badRequest().body("{\"error\":\"La fecha final debe ser mayor a la fecha inicial.\"}");
        }
    }

    @GetMapping(value = "", produces = "application/json")
    public ResponseEntity getWorkedHours(@RequestParam(name = "idtecnico") String idtecnico, @RequestParam(name = "week") String week) throws JsonProcessingException { ;
        return ResponseEntity.ok().body(reporteService.getReporteByTecnicoAndWeek(idtecnico, week));
    }
}
