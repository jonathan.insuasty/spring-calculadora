package ias.talentohumano.calculadora.reporte.controllers;

import ias.talentohumano.calculadora.reporte.models.ServicioModel;
import ias.talentohumano.calculadora.reporte.services.ServiciosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@CrossOrigin
@RestController
@RequestMapping("/servicios")
public class ServicioController {
    @Autowired
    ServiciosService serviciosService;

    @GetMapping(produces = "application/json")
    public ArrayList<ServicioModel> getServicios() {
        return serviciosService.getServicios();
    }
}
